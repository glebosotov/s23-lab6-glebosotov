from bonus_system import calculateBonuses

STANDARD = "Standard"
PREMIUM = "Premium"
DIAMOND = "Diamond"
INVALID = "invalid"


def test_standard_program():
    process_test_with_program(STANDARD)


def test_premium_program():
    process_test_with_program(PREMIUM)


def test_diamond_program():
    process_test_with_program(DIAMOND)


def test_nothing_program():
    process_test_with_program(INVALID)


def process_test_with_program(program):
    if program == STANDARD:
        assert calculateBonuses(STANDARD, 1) == multiply(1, 0.5)
        assert calculateBonuses(STANDARD, 10000) == multiply(1.5, 0.5)
        assert calculateBonuses(STANDARD, 50000) == multiply(2, 0.5)
        assert calculateBonuses(STANDARD, 100000) == multiply(2.5, 0.5)
        assert calculateBonuses(STANDARD, 2000) == multiply(1, 0.5)
        assert calculateBonuses(STANDARD, 15000) == multiply(1.5, 0.5)
        assert calculateBonuses(STANDARD, 60000) == multiply(2, 0.5)
        assert calculateBonuses(STANDARD, 90000) == multiply(2, 0.5)
        assert calculateBonuses(STANDARD, 200000) == multiply(2.5, 0.5)


    if program == PREMIUM:
        assert calculateBonuses(PREMIUM, 1) == multiply(1, 0.1)
        assert calculateBonuses(PREMIUM, 10000) == multiply(1.5, 0.1)
        assert calculateBonuses(PREMIUM, 50000) == multiply(2, 0.1)
        assert calculateBonuses(PREMIUM, 100000) == multiply(2.5, 0.1)
        assert calculateBonuses(PREMIUM, 2000) == multiply(1, 0.1)
        assert calculateBonuses(PREMIUM, 15000) == multiply(1.5, 0.1)
        assert calculateBonuses(PREMIUM, 60000) == multiply(2, 0.1)
        assert calculateBonuses(PREMIUM, 90000) == multiply(2, 0.1)
        assert calculateBonuses(PREMIUM, 200000) == multiply(2.5, 0.1)


    if program == DIAMOND:
        assert calculateBonuses(DIAMOND, 1) == multiply(1, 0.2)
        assert calculateBonuses(DIAMOND, 10000) == multiply(1.5, 0.2)
        assert calculateBonuses(DIAMOND, 50000) == multiply(2, 0.2)
        assert calculateBonuses(DIAMOND, 100000) == multiply(2.5, 0.2)
        assert calculateBonuses(DIAMOND, 2000) == multiply(1, 0.2)
        assert calculateBonuses(DIAMOND, 15000) == multiply(1.5, 0.2)
        assert calculateBonuses(DIAMOND, 60000) == multiply(2, 0.2)
        assert calculateBonuses(DIAMOND, 90000) == multiply(2, 0.2)
        assert calculateBonuses(DIAMOND, 200000) == multiply(2.5, 0.2)

    if program == INVALID:
        assert calculateBonuses(" ", 1) == multiply(1, 0.0)
        assert calculateBonuses("asdasdasdasdasd", 10000) == multiply(1.5, 0.0)
        assert calculateBonuses("xcaz", 50000) == multiply(2, 0.0)
        assert calculateBonuses("INVALID", 100000) == multiply(2.5, 0.0)
        assert calculateBonuses("INVALID", 2000) == multiply(1, 0)
        assert calculateBonuses("INVALID", 15000) == multiply(1.5, 0)
        assert calculateBonuses("INVALID", 60000) == multiply(2, 0)
        assert calculateBonuses("INVALID", 90000) == multiply(2, 0)
        assert calculateBonuses("INVALID", 200000) == multiply(2.5, 0.0)


def multiply(x, y):
    return x * y
